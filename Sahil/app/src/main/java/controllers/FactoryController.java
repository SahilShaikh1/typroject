package controllers;

import android.app.Activity;

import controllers.interfaces.IController;
import controllers.interfaces.ILoginController;
import controllers.masterController.SmsController;
import controllers.masterController.UserController;
import utils.Constant;

/**
 * Created by admin on 30/01/2018.
 */

public class FactoryController {

    public static IController getController (String name, Activity activity){
        switch (name){
            case Constant.SMS:
                return new SmsController(activity);
            default:
                return new UserController(activity);

        }
    }

    public static ILoginController getLoginController(Activity activity){
        return new LoginController(activity);
    }
}
