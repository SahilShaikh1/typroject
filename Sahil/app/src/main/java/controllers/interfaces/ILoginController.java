package controllers.interfaces;

import java.util.List;

/**
 * Created by user on 21-12-2017.
 */

public interface ILoginController  {
    boolean authenticateUser(String emailId,String password);
}
