package controllers.interfaces;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by user on 09-01-2018.
 */

public interface IController<T> {
    void get(String url, String loadMessage);
    void post(String url, T entity, String loadMessage);
    List<T> getAllFromResponse(Class<T[]> clazz, String response );
    T getByIdFromJson(final Class<T> clazz, String response);

    void setMyCallback(Callable call );
}
