package controllers.base;

import android.app.Activity;
import android.content.Context;
import android.util.Log;


import java.util.List;
import java.util.concurrent.Callable;

import Models.base.BaseEntity;
import Models.master.SmsMaster;
import Models.master.UserMaster;
import controllers.interfaces.IController;
import services.AsyncRequest;
import services.REST.IActivityResponse;
import utils.Constant;
import utils.DialogService;
import utils.JsonConversion;

/**
 * Created by user on 21-12-2017.
 */
public class BaseController<T> implements IController<T>, AsyncRequest.IOnAsyncRequestComplete {


    protected IActivityResponse iActivityResponse;
    protected Activity activity;
    protected String url;
    Context context;
    private Callable myCallback;


    public BaseController() {
    }

    public BaseController(Activity activity) {
        iActivityResponse = (IActivityResponse) activity;
        this.activity = activity;
        this.context = activity;
    }

    public void setMyCallback(Callable call) {
        this.myCallback = call;
    }

    public void get(String url, String loadMessage) {
        try {
            new AsyncRequest(this, activity, Constant.GET, loadMessage).execute(url);
        } catch (Exception ex) {
            DialogService.openAlertDialog(activity, "Exception Error", ex.getMessage(), "Ok", false).show();
        }
    }

    public void post(String url, T entity, String loadMessage) {
        try {
            if (entity instanceof BaseEntity) {
                if (setCreatedModifiedBy((BaseEntity) entity)) {

                    String json = new JsonConversion().toJson(entity);
                    new AsyncRequest(this, activity, Constant.POST, json, loadMessage).execute(url);
                    return;
                }
            }

            String json = new JsonConversion().toJson(entity);
            new AsyncRequest(this, activity, Constant.POST, json, loadMessage).execute(url);
        } catch (Exception ex) {
            DialogService.openAlertDialog(activity, "Exception Error", ex.getMessage(), "Ok", false).show();
        }
    }

    private boolean setCreatedModifiedBy(BaseEntity entity) {
        UserMaster userMaster = mainApplication.Application.getInstance().getUser();
        if (userMaster == null) {
            return true;
        } else {
            String user = userMaster .getEmailId() == null ? userMaster .getPassword() : userMaster .getMobileNumber().toString();
            if (entity.getId() == null)
                entity.setCreatedBy(user);
            else
                entity.setModifiedby(user);
        }
        return false;
    }

    // for login activity
    public void postJson(String url, String json, String loadMessage) {
        try {
            new AsyncRequest(this, activity, Constant.POST, json, loadMessage).execute(url);
        } catch (Exception ex) {
            DialogService.openAlertDialog(activity, "Exception Errors", ex.getMessage(), "Ok", false).show();
        }
    }


    @Override
    public void asyncResponse(String response) throws Exception {
        Log.d("ResponseINBASE:", response);
        iActivityResponse.setResponseForActivity(response);
        myCallback.call();

    }

    public List<T> getAllFromResponse(Class<T[]> clazz, String response) {
        try {
            if (response != null && response.trim().charAt(0) == '[')
                return new JsonConversion().getListFromJson(response, clazz);
            else
                DialogService.openAlertDialog(context, response == null ? "No Response from Server" : response, "Back", false).show();
        } catch (Exception ex) {
            DialogService.openAlertDialog(context, "Exception Error", ex.getMessage(), "Back", false).show();
        }
        return null;
    }

    public T getByIdFromJson(final Class<T> clazz, String response) {
        try {
            if (response != null ) {
                return (T) new JsonConversion().fromJson(response, clazz) ;
            } else
                DialogService.openAlertDialog(context, response == null ? "No Response from Server" : response, "Back", false).show();
        } catch (Exception ex) {
            DialogService.openAlertDialog(context, "Exception Error", ex.getMessage(), "Back", false).show();
        }
        return null;
    }
}

