package controllers;

import android.app.Activity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import Models.master.UserMaster;
import controllers.base.BaseController;
import controllers.interfaces.ILoginController;
import utils.ConfigUrl;
import utils.Constant;


/**
 * Created by user on 21-12-2017.
 */

public class LoginController extends BaseController<UserMaster> implements ILoginController {

    public LoginController(Activity activity) {
        super(activity);
    }

    public LoginController() {
        super();
    }

    @Override
    public boolean authenticateUser(String emailId,String password1) {
        userTest user = new userTest(emailId, password1) ;
        String json = new Gson().toJson(user);
        String url = ConfigUrl.LOGIN_API;
        super.postJson( url, json, Constant.AUTHENTICATING);
        return true;
    }

    private class userTest{
        public userTest( String emailId,String password) {
            this.emailId = emailId;
            this.password = password;
        }
        public String  emailId, password;
    }
}
