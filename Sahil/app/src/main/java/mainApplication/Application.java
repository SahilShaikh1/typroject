package mainApplication;

import Models.master.SmsMaster;
import Models.master.UserMaster;

/**
 * Created by admin on 30/01/2018.
 */

public class Application {


    private static Application instance;
    private UserMaster user;
    private String ipAddress;



    public UserMaster getUser() {
        return user;
    }

    public void setUser(UserMaster user) {
        this.user = user;
    }

    public static Application getInstance() {
        if(instance ==null)
            instance = new Application();
        return instance;
    }

    private Application(){

    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
