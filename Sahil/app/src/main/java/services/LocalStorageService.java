package services;

import android.content.Context;
import android.content.SharedPreferences;


import mainApplication.Application;
import utils.Constant;

import static android.provider.Telephony.Carriers.PASSWORD;
/**
 * Created by user on 06-01-2018.
 */

public class LocalStorageService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    public LocalStorageService (Context context){
        sharedPreferences = context.getSharedPreferences(Constant.APP_NAME,0);
        editor =sharedPreferences.edit();
    }

    public void setUsernameAndPassword(String emailId,String password){
        editor.putString(Constant.EMAILID, emailId);
        editor.putString(Constant.PASSWORD, password);
        editor.commit();
    }

    public String getEmailId(){
        return   sharedPreferences.getString(Constant.EMAILID,null);
    }
    public String getPassword(){
        return sharedPreferences.getString(Constant.PASSWORD,null);
    }

    public void setIpAddress(String ipAddress){
        editor.putString(Constant.SERVER_IP,ipAddress);
        editor.commit();
        Application.getInstance().setIpAddress(ipAddress);
    }
    public String getIpAdress(){
        return sharedPreferences.getString(Constant.SERVER_IP, "Ip Address not set");
    }





}
