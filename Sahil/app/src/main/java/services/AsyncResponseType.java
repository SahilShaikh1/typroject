package services;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Arrays;
import java.util.List;

import utils.BooleanTypeAdapter;

/**
 * Created by user on 21-12-2017.
 */

public class AsyncResponseType<T> {

    private String response = null;
    private Context context;

    public AsyncResponseType(Context context, String response) {
        this.context = context;
        this.response = response;
    }

    public AsyncResponseType() {
    }

    public void nullResponse() {
        if(response == null)
            Toast.makeText(context, "No Response from Server", Toast.LENGTH_LONG).show();
    }

    public static final <T> List<T> getList(final Class<T[]> clazz, final String json)
    {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Boolean.class, new BooleanTypeAdapter());
        Gson gson = builder.create();
        final T[] jsonToObject = gson.fromJson(json, clazz);
        return Arrays.asList(jsonToObject);
    }

    public T objectResponse() {
        boolean cond =( response!=null && response.charAt(0)=='{' );
        return cond? (T) response: null;
    }
}