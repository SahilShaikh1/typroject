package services.REST;

import org.json.JSONException;

/**
 * Created by user on 21-12-2017.
 */

public interface IActivityResponse {
    void setResponseForActivity(String response) throws JSONException;
}
