package services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import controllers.base.BaseController;
import utils.Constant;
import utils.DialogService;

public class AsyncRequest extends AsyncTask<String, Integer, String> {

    IOnAsyncRequestComplete onAsyncRequestComplete;
    Context context;
    String httpCallMethod = "POST";
    ProgressDialog pDialog = null;
    String dialogMessage ="Loading";
    Map dataToPost;
    String stringDataToPost;
    String ERROR = null;

    public AsyncRequest(BaseController controller, Activity activity, String postMethod, String dataPost, String loadMessage) {
        onAsyncRequestComplete = (IOnAsyncRequestComplete) controller;
        context = activity;
        httpCallMethod = postMethod;
        stringDataToPost = dataPost;
        dialogMessage = loadMessage;
    }

    public AsyncRequest( BaseController controller, Activity activity, String getMethod,  String loadMessage) {
        onAsyncRequestComplete = (IOnAsyncRequestComplete) controller;
        context = activity;
        httpCallMethod = getMethod;
        dialogMessage = loadMessage;
    }

    public String doInBackground(String... urls) {
        // get url pointing to entry point of API
        String address = urls[0].toString();
        try {
            if (httpCallMethod == Constant.POST) {
                return post(address);
            }
            if (httpCallMethod == Constant.GET) {
                return get(address);
            }
        } catch (Exception ex) {
            ERROR = ex.getMessage();
        }
        return null;
    }

    public void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(dialogMessage); // typically you will define such
        // strings in a remote file.
        pDialog.show();
    }

    public void onProgressUpdate(Integer... progress) {
        // you can implement some progressBar and update it in this record
        // setProgressPercent(progress[0]);
    }

    public void onPostExecute(String response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        if (ERROR != null) {
            DialogService.openAlertDialog(context, "Error", ERROR, "Back", false).show();
            return;
        }
        try {
            onAsyncRequestComplete.asyncResponse(response);
        } catch (Exception e) {
            ERROR = e.getMessage();
        }

    }

    protected void onCancelled(String response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        try {
            onAsyncRequestComplete.asyncResponse(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private String get(String address) {
        try {
            URL url = new URL(address);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            conn.setConnectTimeout(20000);
            Log.d("ResponseCode: ", String.valueOf(conn.getResponseCode()) + " Response Message: " + conn.getResponseMessage());


            if (conn.getResponseCode() != 200) {
                throw new Exception("Failed : HTTP error code :"
                        + conn.getResponseCode() + ", " + conn.getResponseMessage());
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            String line = null;
            StringBuilder sb = new StringBuilder();
            String result = null;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            bufferedReader.close();
            result = sb.toString();
            return result;

        } catch (Exception e) {
            ERROR = e.getMessage();
            return ERROR;
        }
    }

    private String post(String address) throws Exception {
        URL url = new URL(address);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setConnectTimeout(20000);

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        Log.d("JSON IN BACKGROUNG", stringDataToPost);
        writer.write(stringDataToPost);
        writer.close();
        os.close();

        String line = null;
        StringBuilder sb = new StringBuilder();
        String result = null;
        if(conn.getResponseCode() == 403){
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            bufferedReader.close();
            result = sb.toString();
            throw new Exception(result);
        }

        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line);
        }

        bufferedReader.close();
        result = sb.toString();

        return result;

    }

    // Interface to be implemented by calling activity
    public interface IOnAsyncRequestComplete {
        void asyncResponse(String response) throws Exception;
    }
}