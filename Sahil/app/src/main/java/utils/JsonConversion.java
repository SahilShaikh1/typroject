package utils;

import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

/**
 * Created by user on 09-01-2018.
 */

public class JsonConversion<T>{

    public JsonConversion() {
    }

    public String toJson(T entity) {
        Gson gson = new Gson();
        return gson.toJson(entity);
    }

    public String toJson(T entity, boolean serializeNull) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(entity);
    }

    public <T> List<T> getListFromJson(final String json, final Class<T[]> clazz) {
        if (json == null || json.isEmpty()) {
            System.out.println("Null Response in getListFromJson()");
            return null;
        }
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Boolean.class, new BooleanTypeAdapter());
            builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
            Gson gson = builder.create();
            final T[] jsonToObject = gson.fromJson(json, clazz);
            return Arrays.asList(jsonToObject);
        } catch (JsonSyntaxException ex){
            Log.d("ERROR ", "JSON SYNTAX EXCEPTION Jsonconversion.java");
        }
        return null;
    }

    public T fromJson(String json, Class<T> type) {
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Boolean.class, new BooleanTypeAdapter());
            Gson gson = builder.create();
            return gson.fromJson(json, type);
        } catch (Exception ex) {
            Log.d("ERROR ", "JSON SYNTAX EXCEPTION Jsonconversion.java");
            Log.d("ERROR ", ex.getMessage());
            System.out.println(ex);
            return null;
        }
    }
}
