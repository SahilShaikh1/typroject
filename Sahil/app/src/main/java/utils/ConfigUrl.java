package utils;

public class ConfigUrl {

   // public static final String SAVE_SMS = "http://192.168.43.131:7086/api/common/saveSmsMaster";
    public static final String MAIN_URL = "http://192.168.0.100:7086/api/common";
    public static final String SAVE_SMS =  MAIN_URL + "/saveSmsMaster";
    public static final String SAVE_USER = MAIN_URL + "/saveUserMaster";
    public static final String LOGIN_API = MAIN_URL + "/getUser";
    public static final String CHECK_USER = MAIN_URL + "/checkUser";

}
