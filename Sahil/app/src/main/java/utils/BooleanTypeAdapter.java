package   utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by user on 21-12-2017.
 */
public class BooleanTypeAdapter implements JsonDeserializer<Boolean> {
    public Boolean deserialize(JsonElement json, Type typeOfT,
                               JsonDeserializationContext context) throws JsonParseException {
        if (!(json.toString().equals("true") || json.toString().equals("false"))) {
            int code = json.getAsInt();
            return code == 0 ? false :
                    code == 1 ? true :
                            null;
        }
        return json.toString().equals("true") ? true : false;
    }
}
