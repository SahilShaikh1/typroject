package  utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import mainApplication.Application;


/**
 * Created by user on 21-12-2017.
 */

public class DialogService extends AppCompatActivity {

    public static AlertDialog openLogOutDialog(final Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you really want to Logout? ")
                .setTitle("Logout") ;

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                Intent loginscreen = new Intent(context, LoginActivity.class);
//                loginscreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                Application.getInstance().setUser(null);
//                context.startActivity(loginscreen);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                // User clicked cancel button
            }
        });
        return builder.create();
    }


    public static AlertDialog forceLogout(final Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Unauthorized user found, Please login again")
                .setTitle("Unauthorized") ;

        builder.setPositiveButton("Login again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                Intent loginscreen = new Intent(context, LoginActivity.class);
//                loginscreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                context.startActivity(loginscreen);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                // User clicked cancel button
            }
        });
        return builder.create();
    }

    public static AlertDialog openAlertDialog(final Context context, String message, String positivBtnText,
                                              boolean  showNegativeBtn)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message) ;

        builder.setPositiveButton(positivBtnText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });

        if(showNegativeBtn)
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    // User clicked cancel button
                }
            });
        return builder.create() ;
    }

    public static Boolean openAlertDialog(final Context context, String message, String positivBtnText,
                                          boolean  showNegativeBtn , String negativeBtnText)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message) ;

        builder.setPositiveButton(positivBtnText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        if(showNegativeBtn)
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    // User clicked cancel button
                }
            });
         builder.create().show();
        return true;
    }

    public static AlertDialog openAlertDialog(final Context context,String titlemsg, String message,
                                              String positivBtnText, boolean  showNegativeBtn)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setTitle(titlemsg) ;

        builder.setPositiveButton(positivBtnText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });

        if(showNegativeBtn)
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    // User clicked cancel button
                }
            });
        return builder.create();
    }

}
