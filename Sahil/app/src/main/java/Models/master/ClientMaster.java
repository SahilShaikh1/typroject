package Models.master;

import Models.base.BaseEntity;

public class ClientMaster extends BaseEntity {

    private String name;
    private String emailId;
    private String mobileNumber;
    private String address;
    private String dateOfBirth;
    private String lastEmailSent;
    private String lastSmsSent;
    private Boolean isChecked;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLastEmailSent() {
        return lastEmailSent;
    }

    public void setLastEmailSent(String lastEmailSent) {
        this.lastEmailSent = lastEmailSent;
    }

    public String getLastSmsSent() {
        return lastSmsSent;
    }

    public void setLastSmsSent(String lastSmsSent) {
        this.lastSmsSent = lastSmsSent;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}
