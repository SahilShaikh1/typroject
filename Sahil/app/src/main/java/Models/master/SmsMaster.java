package Models.master;

import Models.base.BaseEntity;
import Models.transaction.ModelEnums;

public class SmsMaster extends BaseEntity {


    private String content;
    private ModelEnums.SmsType smsType;
    private String senderId;

    public SmsMaster() {
    }

    public ModelEnums.SmsType getSmsType() {
        return smsType;
    }

    public void setSmsType(ModelEnums.SmsType smsType) {
        this.smsType = smsType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
