package Models.transaction;

public class ModelEnums {

    public enum EmailType {
        FESTIVAL_WISHES,
        SOCIAL,
        WELCOME,
        OFFER,
        ANNIVERSARY,
        BIRTHDAY
    }

    public enum SmsType {
        FESTIVAL_SMS,
        SOCIAL,
        WELCOME,
        OFFER,
        BIRTHDAY,
        ANNIVERSARY
    }


    public enum scheduledType {
        DAILY,
        WEEKLY,
        MONTHLY,
        ONE_IN_FIFTEEN_DAY
    }

    public enum subscriptionType {
        THREE_MONTHS,
        SIX_MONTHS,
        TWELVE_MONTHS;

    }
}