//package com.example.sahil.sahil;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//
//import org.json.JSONException;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.Callable;
//
//import Models.master.SmsMaster;
//import Models.transaction.ModelEnums;
//import controllers.FactoryController;
//import controllers.interfaces.IController;
//import controllers.masterController.SmsController;
//import services.REST.IActivityResponse;
//import utils.ConfigUrl;
//import utils.Constant;
//import utils.DialogService;
//
//public class Sms extends BaseNavActivity implements View.OnClickListener, IActivityResponse {
//
//    Button RequestButton; // button which on clicking, sends the request
//    TextView DisplayText; // a text field to display the request response
//    EditText content, smsType, senderId; // a text field where the data to be sent is entered
//    private SmsMaster entity;
//    private List<Sms> smsList = new ArrayList<>();
//    private IController smsController;
//    String response;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View contentView = inflater.inflate(R.layout.sms, null, false);
//        drawerLayout.addView(contentView, 0);
//        entity = new SmsMaster();
//        smsController = new SmsController(this);
//
//        RequestButton = (Button) findViewById(R.id.RequestButton);
//        content = (EditText) findViewById(R.id.content);
//        smsType = (EditText) findViewById(R.id.smsType);
//        senderId = (EditText) findViewById(R.id.senderId);
//        DisplayText = (TextView) findViewById(R.id.DisplayText);
//        RequestButton.setOnClickListener(this);
//        //final String url = "http://10.0.2.2:3000/saveSmsMaster"; // your URL
//
//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            Integer in = bundle.getInt(Constant.ID);
//            if (in != null) {
//                final IController contr = FactoryController.getController(Constant.SMS, this);
//                contr.setMyCallback(new Callable() {
//                    @Override
//                    public Object call() throws Exception {
//                        SmsMaster sms = (SmsMaster) contr.getByIdFromJson(SmsMaster.class, response);
//                        if (sms != null) {
//                            entity = sms;
//                        }
//
//                        return null;
//                    }
//                });
//                contr.get(ConfigUrl.SAVE_SMS + in, Constant.FETCHING_DATA);
//            }
//        }
//    }
//
//    @NonNull
//    private Callable getCall(final String tag) {
//        return new Callable() {
//            @Override
//            public Object call() throws Exception {
//                switch (tag) {
//                    case Constant.SMS:
//                        smsList = smsController.getAllFromResponse(SmsMaster[].class, response);
//                        break;
//                }
//                return null;
//            }
//        };
//    }
//
//    public void save() {
//        entity.setId(0);
//        entity.setContent(content.getText().toString());
//        entity.setSmsType(ModelEnums.SmsType.FESTIVAL_SMS);
//        entity.setSenderId(senderId.getText().toString());
//        smsController.setMyCallback(new Callable() {
//            @Override
//            public Object call() throws Exception {
//                if (response != null) {
//                    DialogService.openAlertDialog(Sms.this, "Saved Successfully", "Ok", false).show();
//                }
//                return null;
//            }
//        });
//        smsController.post(ConfigUrl.SAVE_SMS, entity, "MSG SAVED");
//
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//        switch (id) {
//            case R.id.RequestButton:
//                save();
//                break;
//
//        }
//    }
//
//    @Override
//    public void setResponseForActivity(String response) throws JSONException {
//        this.response = response;
//    }
//}
