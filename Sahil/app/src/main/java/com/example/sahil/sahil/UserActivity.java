package com.example.sahil.sahil;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import Models.master.UserMaster;
import controllers.FactoryController;
import controllers.interfaces.IController;
import controllers.masterController.UserController;
import mainApplication.Application;
import services.REST.IActivityResponse;
import utils.ConfigUrl;
import utils.Constant;
import utils.DialogService;
import utils.JsonConversion;

public class UserActivity extends BaseNavActivity implements View.OnClickListener, IActivityResponse {
    TextView BackToLogin;
    Button save; // button which on clicking, sends the request
    TextView DisplayText; // a text field to display the request response
    EditText etEmailId, etPassword, etMobileNumber,etConfirmPassword; // a text field where the data to be sent is entered
    private UserMaster entity;
    private List<UserActivity> userList = new ArrayList<>();
    private IController userController;
    Boolean isExist;
    String response;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initUI();
        entity = new UserMaster();
        userController = new UserController(this);
        //final String url = "http://10.0.2.2:3000/saveSmsMaster"; // your URL

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            Integer in = bundle.getInt(Constant.ID);
            if(in!=null){
                final IController contr = FactoryController.getController(Constant.USER,this);
                contr.setMyCallback(new Callable() {
                    @Override
                    public Object call() throws Exception {
                        UserMaster user = (UserMaster) contr.getByIdFromJson(UserMaster.class, response);
                        if(user != null){
                            entity = user;
                        }

                        return null;
                    }
                });
                contr.get(ConfigUrl.SAVE_USER + in, Constant.FETCHING_DATA);
            }
        }
    }

    private void initUI(){
        save = (Button) findViewById(R.id.save);
        etEmailId = (EditText) findViewById(R.id.emailId);
        etPassword = (EditText) findViewById(R.id.password);
        etConfirmPassword = findViewById(R.id.confirmPassword);
        etMobileNumber = (EditText) findViewById(R.id.mobileNumber);
//        DisplayText = (TextView) findViewById(R.id.DisplayText);
        BackToLogin = findViewById(R.id.backToLogin);
        save.setOnClickListener(this);
        BackToLogin.setOnClickListener(this);

    }
    private void validate() {

        etEmailId.setError(null);
        etPassword.setError(null);
        etMobileNumber.setError(null);


        String email = etEmailId.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        String mobileNumber = etMobileNumber.getText().toString();
        boolean cancel = false;
        View focusView = null;
        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            etPassword.setError(getString(R.string.error_field_required));
            focusView = etPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            etEmailId.setError(getString(R.string.error_field_required));
            focusView = etEmailId;
            cancel = true;
        }
        if (TextUtils.isEmpty(mobileNumber)) {
            etMobileNumber.setError(getString(R.string.error_field_required));
            focusView = etMobileNumber;
            cancel = true;
        }
        if(TextUtils.isEmpty(confirmPassword)) {
                etConfirmPassword.setError(getString(R.string.error_field_required));
                focusView = etConfirmPassword;
                cancel = true;
            }
        if (confirmPassword == password){
            etConfirmPassword.setError(getString(R.string.error_confirm_password));
            focusView = etConfirmPassword;
            cancel = true;
        }



        if (cancel) {
            focusView.requestFocus();
        } else {
            save();
        }
    }
    @NonNull
    private Callable getCall(final String tag) {
        return new Callable() {
            @Override
            public Object call() throws Exception {
                switch (tag) {
                    case Constant.USER:
                        userList = userController.getAllFromResponse(UserMaster[].class, response);
                        break;
                }
                return null;
            }
        };
    }

    public void save() {
        entity.setId(0);
        //entity.setUserId(getUserId(this));
        entity.setSubscribed(false);
        entity.setEmailId(etEmailId.getText().toString());
        entity.setPassword(etPassword.getText().toString());
        entity.setMobileNumber(Integer.parseInt(etMobileNumber.getText().toString()));
        userController.setMyCallback(new Callable() {
            @Override
            public Object call() throws Exception {

                if (response != null) {
                    DialogService.openAlertDialog(UserActivity.this, "Saved Successfully", "Ok", false).show();
                }
                return null;
            }
        });
      userController.post(ConfigUrl.SAVE_USER,entity, "MSG SAVED");

//        userController.post(ConfigUrl.CHECK_USER,entity,"Checking User...");
//        if(!isExist){
//            userController.post(ConfigUrl.SAVE_USER,entity, "MSG SAVED");
//        }else {
//            Toast.makeText(this,"User Already Exist",Toast.LENGTH_LONG);
//        }



    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.save:
                validate();
                break;
            case R.id.backToLogin:
                Intent intent = new Intent(this,LoginActivity.class);
                startActivity(intent);

        }
    }
    @Override
    public void setResponseForActivity(String response) throws JSONException {
        this.response = response;

    }
}
