package com.example.sahil.sahil;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sahil.sahil.R;

import org.json.JSONException;

import Models.master.UserMaster;
import controllers.FactoryController;
import controllers.interfaces.ILoginController;
import mainApplication.Application;
import services.LocalStorageService;
import services.REST.IActivityResponse;
import utils.Constant;
import utils.JsonConversion;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, IActivityResponse {



    private EditText etUsername, etPassword;
    private Button btnSubmit;
    private TextView txtRegistration;
    private TextView tvIpAddress;
    private LocalStorageService storageService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUI();
        initObj();
       // setUserNameAndPasswordFromStorage();
//        setIpAdress();

    }


    private void initObj() {
        storageService = new LocalStorageService(this);
    }

    private void initUI() {
        etUsername =findViewById(R.id.emailId);
        etPassword =findViewById(R.id.loginPassword);
        btnSubmit = findViewById(R.id.login);
        txtRegistration = findViewById(R.id.registration);
//      tvIpAddress = (TextView)findViewById(R.id.tvIpAddress);
        btnSubmit.setOnClickListener(this);
        txtRegistration.setOnClickListener(this);
//      tvIpAddress.setOnClickListener(this);

    }

//    private void setUserNameAndPasswordFromStorage() {
//        String userName = storageService.getEmailId();
//        String password = storageService.getPassword();
//        if(userName!=null && password!=null){
//            etUsername.setText(userName);
//            etPassword.setText(password);
//        }
//    }
//    private void setIpAdress(){
//        String ipAddress = storageService.getIpAdress();
//        Application.getInstance().setIpAddress(ipAddress);
//        tvIpAddress.setText(ipAddress);
//        Constant.SERVER_IP = ipAddress;
//    }

    private void attemptLogin() {

        etUsername.setError(null);
        etPassword.setError(null);

        String email = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        boolean cancel = false;
        View focusView = null;
        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            etPassword.setError(getString(R.string.error_invalid_password));
            focusView = etPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            etUsername.setError(getString(R.string.error_field_required));
            focusView = etUsername;
            cancel = true;
        }


        if (cancel) {
            focusView.requestFocus();
        } else {
            authenticateUser();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.tvIpAddress:
//                AlertDialog.Builder alert = new AlertDialog.Builder(this);
//                final EditText editText = new EditText(this);
//                alert.setTitle("IP Address Configuration");
//                alert.setMessage("Enter IP Address");
//                alert.setView(editText);
//                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String ipAddress = editText.getText().toString();
//                        tvIpAddress.setText(ipAddress);
//                        storageService.setIpAddress(ipAddress);
//                         Application.getInstance().setIpAddress(ipAddress);
//                    }
//                });
//                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                alert.show();
//                break;
            case R.id.login:
                attemptLogin();
                break;
            case R.id.registration:
                Intent intent = new Intent(this,UserActivity.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    public void setResponseForActivity(String response) throws JSONException {
        if(response.equals("")) {
//        if (response.equals("{\"success\":false}")) {
              Toast.makeText(this, "Invalid Credentials ", Toast.LENGTH_LONG).show();
//          }
        } else {
            storageService.setUsernameAndPassword(etUsername.getText().toString(),
                    etPassword.getText().toString());
            Log.d("SUCCESSFULL", ":..............");
            UserMaster user = new JsonConversion<UserMaster>().fromJson(response, UserMaster.class);
            Application.getInstance().setUser(user);

            Intent intent = new Intent(this, Gmails.class);
            startActivity(intent);
        }
    }

    public void authenticateUser( ) {
        ILoginController iLoginController = FactoryController.getLoginController(LoginActivity.this);
        String emailId = etUsername.getText().toString();
        String password =  etPassword.getText().toString();
        iLoginController.authenticateUser(emailId, password);
    }

}
