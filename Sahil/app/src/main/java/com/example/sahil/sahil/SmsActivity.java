package com.example.sahil.sahil;


import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import utils.AlarmService;

public class SmsActivity extends AppCompatActivity {


    String smsNumber, smsText;
    TimePicker tp;
    private PendingIntent pendingIntent;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        ActivityCompat.requestPermissions(SmsActivity.this,
                new String[]{Manifest.permission.READ_CONTACTS},
                1);

        final EditText edittextSmsNumber = (EditText) findViewById(R.id.smsnumber);
        final EditText edittextSmsText = (EditText) findViewById(R.id.smstext);

        Button buttonStart = (Button) findViewById(R.id.startalarm);
        Button buttonCancel = (Button) findViewById(R.id.cancelalarm);
        tp = (TimePicker) findViewById(R.id.time);


        buttonStart.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                smsNumber = edittextSmsNumber.getText().toString();
                smsText = edittextSmsText.getText().toString();

                Intent myIntent = new Intent(SmsActivity.this, AlarmService.class);

                Bundle bundle = new Bundle();
                bundle.putCharSequence("extraSmsNumber", smsNumber);
                bundle.putCharSequence("extraSmsText", smsText);
                myIntent.putExtras(bundle);

                pendingIntent = PendingIntent.getService(SmsActivity.this, 0, myIntent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                int hour = tp.getCurrentHour();
                int min = tp.getCurrentMinute();

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.add(Calendar.MINUTE, min);
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);


                Toast.makeText(SmsActivity.this,
                        "Start Alarm with \n" +
                                "smsNumber = " + smsNumber + "\n" +
                                "smsText = " + smsText,
                        Toast.LENGTH_LONG).show();

            }
        });

        buttonCancel.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(SmsActivity.this, "Cancel!", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {

                    Toast.makeText(SmsActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }

    }
}