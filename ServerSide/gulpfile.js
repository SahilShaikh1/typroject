var gulp =require('gulp');
var nodemon = require('gulp-nodemon');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
var minify = require('gulp-minify'); 

gulp.task('default',function(){
	gulp.src('lib/*.js')
	.pipe(minify({
		ext:{
			src:'debug.js',
			min:'.js'
		},
		exclude: ['task'],
		ignoreFiles:['.combo.js','-min.js']
	}))
	.pipe(gulp.dest('dist'))
	livereload.listen();
	nodemon({
		script: 'bin/www',
		ext:  'js jade html css styl json'
	}).on('restart', function(){
		gulp.src('bin/www')
		.pipe(livereload())
		.pipe(notify('Reloading page, please wait...'));
	})
})