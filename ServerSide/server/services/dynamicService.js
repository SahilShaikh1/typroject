 var empModel = require("../models");
 var moment = require("moment"); 

 module.exports = function (sequelize, model) {  
 	var service ={ 
 		getBySQLQuery:getBySQLQuery
 	} 
 	return service; 

 	function getBySQLQuery(query){
 		return sequelize().sequelize.query(query); 
 	}
}