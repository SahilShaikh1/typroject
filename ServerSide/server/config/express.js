var express, logger, cookieParser, bodyParser, stylus, session, passport, sitemap;
express = require('express');
logger = require('morgan');
cookieParser = require('cookie-parser');
bodyParser = require('body-parser');
stylus = require('stylus');
session = require('express-session');
passport = require('passport');
path = require('path')
fs = require('fs');

module.exports =function (app,config) {
	function compile(str,path){
		return stylus(str).set('filename',path);
	}
   // var staticRoot = 'D:\\securityAnalysisReplica\\public\\';
    
    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
        
        var accept = req.accepts('html', 'json', 'xml');
        if(accept !== 'html'){
            return next();
        }
        var ext = path.extname(req.path);
        if (ext !== ''){
            return next();
        }
        if ('OPTIONS' == req.method) {
         res.send(200);
     } else {
      next();
  }


});

   // app.use(express.static(staticRoot));

    
    app.use(logger('dev'));
    app.use(cookieParser());

    app.use(bodyParser.json({limit: "50mb"}));
    app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
    app.use(session({secret: 'Sahil Shaikh'}));
    app.use(passport.initialize());
    app.use(passport.session()); 

    var timeout = require('connect-timeout');

    app.use(timeout(300000));  

    app.use(stylus.middleware(
    {
        src: config.rootPath + 'public',
        compile: compile
    }
    ));
}
