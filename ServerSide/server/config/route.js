var model = require("../models");
var dynamicRouter = require("../route/dynamicRouter.js");  

module.exports = function (app, config) {    
    app.get('/logout', function(req, res){ 
        if (req.session) {
            req.session.destroy(function() {});
          }
          res.redirect('/startPage');
    });   
    
    app.use('/api/common', dynamicRouter);

   

}