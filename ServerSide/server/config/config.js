var path = require('path');
var rootPath=path.normalize(__dirname+'/../../');


console.log(rootPath);

module.exports={
	development:{
		rootPath:rootPath,
		port:process.env||3030
	},
	 production:{
        rootPath:rootPath,
        port:process.env.PORT ||8083
    }
}