 module.exports = function () { 
	var env       = process.env.NODE_ENV; 
	var path      = require("path"); 
	var config    = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

	 var moment = require("moment"); 

	var Sequelize = require("sequelize");

	console.log(config)
	
	var sequelize =  new Sequelize(config.database, config.username, config.password, {
		host: config.host,
		dialect: config.dialect, 
		dialectOptions: {
			options:{
			requestTimeout: 60000000,
			connectTimeout: 600000 
		}
		} ,
		pool: {
			 max: 10,
			 min: 0, 
			 idle: 200000,  
			 acquire: 100000
		} 
	});   
 
    return {sequelize:sequelize,
    	Sequelize:Sequelize
    };

}
