module.exports = function (sequelize, DataType) {
	var CommonModel = sequelize
						.define("CommonModel",  {
							 userId:{ type:DataType.INTEGER, field: 'userId'}
						},
						{
							tableName:'CommonModel'
						});

	sequelize.sync();				

	return CommonModel;
} 
