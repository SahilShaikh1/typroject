 "use strict";

 var fs = require("fs");
 var dir = require('node-dir');
 var util = require('util');

 var path = require("path");
 var seq = require(path.join(__dirname, '..', 'config','sequelize.js'));
 var sequelize=seq().sequelize;
 var Sequelize=seq().sequelize;

 var db  = {};

console.log(__dirname);

var files=dir.files(__dirname, {sync:true}) 
  for (var i = 0, len = files.length; i < len; i++) {
  if(path.basename(files[i])!=="index.js" )
  { 
      var model = sequelize.import(files[i]); 
	  db[model.name] = model;
  }
}
 Object.keys(db).forEach(function(modelName) {
	
	if ("associate" in db[modelName]) {  
		db[modelName].associate(db);

		
	}
 });

 db.sequelize = sequelize.sequelize;
 db.Sequelize = Sequelize; 
 
 module.exports = db;