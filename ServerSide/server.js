const express = require('express'),
	  localStratergy = require('passport-local').Strategy;

	  const env = process.env.Node_ENV =process.env.Node_ENV ||'production';
	  const app = express();

	  var config = require('./server/config/config')[env];

	  require('./server/config/express')(app, config);

	  require('./server/models/index.js');

	  require('./server/config/route.js')(app,config);


	  function clientErrorHandler(err,req,res,next){
	  	if(req.xhr){
	  		res.status(500).send({error:'Something Failed'});
	  	}else{
	  		next(err);
	  	}
	  }	  
	  app.use(clientErrorHandler);
	  module.exports =app;